# import json

from django.db import connection
# from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
from django.db.models import Count, Sum

from ipl.models import Matches, Deliveries


# Create your views here.
def basic_test_view(request):
    # import pdb; pdb.set_trace()
    data = Matches.objects.all()
    return JsonResponse({'data': serializers.serialize('json', data)})

# def get_query_one(request):
#     import pdb; pdb.set_trace()
#     data = Matches.objects.raw('select season, count(season) as number_of_matches from matches group by season order by season;')
#     return JsonResponse({'data' : serializers.serialize('json' , data)})


def year_and_wins_c(request):
    # import pdb; pdb.set_trace()
    cursor = connection.cursor()
    cursor.execute('select season, count(season) as number_of_matches from Matches group by season order by season;')
    data = cursor.fetchall()
    return JsonResponse({'data': data})


def team_and_wins_c(request):
    # import pdb; pdb.set_trace()
    cursor = connection.cursor()
    cursor.execute('select season, winner, count(winner) as count from matches group by winner,season order by season;')
    data = cursor.fetchall()
    length = len(data)
    return JsonResponse({"data": data, 'count': length})


def extra_runs_c(request):
    # import pdb; pdb.set_trace()
    cursor = connection.cursor()
    cursor.execute('select batting_team , sum(extra_runs) as extras from matches left outer join deliveries on matches.id = deliveries.match_id where season = 2016 group by batting_team;')
    return JsonResponse({'data': cursor.fetchall()})


def best_economy_c(request):
    # import pdb; pdb.set_trace()
    cursor = connection.cursor()
    cursor.execute('select bowler , 6*sum(total_runs)/count(ball) as runs_given from matches inner join deliveries on matches.id = deliveries.match_id where season = 2015 group by bowler order by runs_given limit 10;')
    return JsonResponse({'data': cursor.fetchall()})


def year_and_wins(request):
    # import pdb; pdb.set_trace()
    data = Matches.objects.values('season').annotate(number_of_matches=Count('season')).order_by('season')
    return JsonResponse({'data': data})


def team_and_wins(request):
    # import pdb; pdb.set_trace()
    data = Matches.objects.values('season', 'winner').annotate(number_of_runs=Sum('season')).order_by('season')
    return JsonResponse({'data': data})


def extra_runs(request):
    # import pdb; pdb.set_trace()
    data = Deliveries.objects.values('bowling_team').annotate(extra_runs=Sum('extra_runs')).filter(match__season=2016)
    return JsonResponse({'data': data})


def best_economy(request):
    # import pdb; pdb.set_trace()
    data = Deliveries.objects.values('bowler').annotate(economy=6*Sum('total_runs')/Count('ball')).filter(match__season=2015).order_by('economy')
    return JsonResponse({'data': data})






