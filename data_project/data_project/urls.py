"""data_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from ipl import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', views.basic_test_view),
    path('one/', views.year_and_wins_c),
    path('two/', views.team_and_wins_c),
    path('three/', views.extra_runs_c),
    path('four/', views.best_economy_c),
    path('onea/', views.year_and_wins),
    path('twoa/', views.team_and_wins),
    path('threea/', views.extra_runs),
    path('foura/', views.best_economy),
]
