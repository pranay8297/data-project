import psycopg2
import pdb


class Database_ops:
    def __init__(self):
        self.cursor = None

    def get_cursor_obj(self):
        try:
            connection = psycopg2.connect(user='apple',
                                          password='apple',
                                          host='localhost',
                                          port='5432',
                                          database='new_ipl')
            cursor = connection.cursor()
            print(connection.get_dsn_parameters(), '\n')
            return cursor
        except:
            print('cursor couldnt be created')
            return False

    def execute_query(self, string):
        pdb.set_trace()
        self.cursor.execute(string)
        return self.cursor.fetchall()

    def year_and_wins(self):
        string = 'select season, count(season) as number_of_matches from matches group by season order by season;'
        data = self.execute_query(string)
        # import pdb; pdb.set_trace()
        return data

    def team_and_wins(self):
        # self.cursor.execute('select season, winner, count(winner) as count from matches group by winner,season order by season;')
        # data = self.cursor.fetchall()
        query = 'select season, winner, count(winner) as count from matches group by winner,season order by season;'
        data = self.execute_query(query)
        return data

    def extra_runs(self):
        # self.cursor.execute('select batting_team , sum(extra_runs) as extras from matches left outer join deliveries on matches.id = deliveries.match_id where season = 2016 group by batting_team;')
        # data = self.cursor.data
        query = "select bowling_team , sum(extra_runs) as extras from matches inner join deliveries on matches.id = deliveries.match_id where season = 2016 group by bowling_team;"
        data = self.execute_query(query)
        return data

    def best_economy(self):
        query = 'select bowler , 6*sum(total_runs)/count(ball) as runs_given from matches full join deliveries on matches.id = deliveries.match_id where season = 2015 group by bowler order by runs_given limit 10;'
        data = self.execute_query(query)
        return data


